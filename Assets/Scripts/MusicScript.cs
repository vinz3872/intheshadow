﻿using UnityEngine;
using System.Collections;

public class MusicScript : MonoBehaviour {

	private AudioClip		MainMusic;
	private AudioSource		source;

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();
//		source = GameObject.Find ("Main Camera").GetComponent<AudioSource> ();
		MainMusic = Resources.Load ("Musics/MainTheme") as AudioClip;
		source.clip = MainMusic;
		DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		if (source.isPlaying && PlayerPrefs.GetInt ("music") == 0) {
			source.Stop();
		}
		else if (!source.isPlaying){
			source.Play();
		}
	}
}
