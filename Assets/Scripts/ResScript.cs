﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResScript : MonoBehaviour {

	private Toggle		full;
	private Dropdown	option;
	public bool			type = false;
	private Resolution	actualRes;
	[HideInInspector]
	public ResScript	res;

	// Use this for initialization
	void Start () {
		if (type == false) {
			option = GetComponent<Dropdown> ();
			res = GameObject.Find("FullScreenToggle").GetComponent<ResScript>();
		} else {
			full = GetComponent<Toggle> ();
			full.isOn = Screen.fullScreen;
		}
		actualRes.width = Screen.width;
		actualRes.height = Screen.height;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ChangeRes() {
		if (option.value == 0) {
			Screen.SetResolution (1024, 768, Screen.fullScreen);
			res.actualRes.width = 1024;
			res.actualRes.height = 768;
		} else if (option.value == 1) {
			Screen.SetResolution (1280, 720, Screen.fullScreen);
			res.actualRes.width = 1280;
			res.actualRes.height = 720;
		} else if (option.value == 2) {
			Screen.SetResolution(1600, 900, Screen.fullScreen);
			res.actualRes.width = 1600;
			res.actualRes.height = 900;
		}
	}

	public void ChangeResFull() {
//		Resolution res = Screen.currentResolution;
		Screen.SetResolution(actualRes.width, actualRes.height, full.isOn);
	}

}
