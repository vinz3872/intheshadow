﻿using UnityEngine;
using System.Collections;

public class ParticleScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (particleLife());
	}


	IEnumerator particleLife() {
		yield return new WaitForSeconds (1);
		GameObject.Destroy (gameObject);
	}
}
