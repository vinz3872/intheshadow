﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {
	
	public enum ObjType
	{
		TEAPOT,
		ELEPHANT,
		FORTYTWO,
		MAINMENU,
		TESTMENU,
		MENU,
		BUTTON,
		OPTION,
		BONUSLVL,
	}
	
	public ObjType			type;
	public int				rotationAllowed = 1;
	private GameObject		obj;
	private GameObject		obj1 = null;
	private Button[]		levelList = new Button[3]; 
	private bool			win = false;
	private GameObject		winGame;
	private int				detection = 0;
	public int				maxDetection = 0;
	private int				current = 0;
	private GameObject		unlock = null;
	
	// Use this for initialization
	void Start () {
		float x = 0;
		float y = 0;

		obj = GameObject.FindGameObjectWithTag("Player") as GameObject;
		obj1 = GameObject.FindGameObjectWithTag("Player1") as GameObject;
		if (type == ObjType.TEAPOT) {
			y = 97.0f;
			obj.transform.Rotate (new Vector3 (0, -y, 0), Space.World);
		} else if (type == ObjType.ELEPHANT) {
			x = 97.0f;
			obj.transform.Rotate (new Vector3 (x, 0, 0), Space.World);
		} else if (type == ObjType.FORTYTWO) {
			obj = obj.transform.parent.gameObject;
			obj1 = obj1.transform.parent.gameObject;
			x = 97.0f;
			obj.transform.Rotate (new Vector3 (x, 0, 0), Space.World);
			obj.transform.position += new Vector3 (2, 0, 0);
			y = 97.0f;
			obj1.transform.Rotate (new Vector3 (-x, -y, 0), Space.World);
		} else if (type == ObjType.BONUSLVL) {
			y = 97.0f;
			obj.transform.Rotate (new Vector3 (0, -y, 0), Space.World);
			x = 97.0f;
			obj.transform.Rotate (new Vector3 (x, 0, 0), Space.World);
		}
		if (type == ObjType.MENU) {
			colorButton ();
			if (PlayerPrefs.GetInt ("unlock") > 0) {
				unlock = Resources.Load("Prefabs/unlock") as GameObject;
			}
			if (PlayerPrefs.GetInt ("unlock") == 2) {
				GameObject.Instantiate(unlock, new Vector3(0.55f, 11, -3.67f), Quaternion.identity);
				PlayerPrefs.SetInt ("unlock", 0);
			} else if (PlayerPrefs.GetInt ("unlock") == 3) {
				GameObject.Instantiate(unlock, new Vector3(-2.45f, 11, -3.67f), Quaternion.identity);
				PlayerPrefs.SetInt ("unlock", 0);
			} else if (PlayerPrefs.GetInt ("unlock") == 4) {
				GameObject.Instantiate(unlock, new Vector3(-4.45f, 11, -3.67f), Quaternion.identity);
				PlayerPrefs.SetInt ("unlock", 0);
			}
		} else if (type == ObjType.MAINMENU) {
			if (!PlayerPrefs.HasKey ("level1"))
				PlayerPrefs.SetInt ("level1", 1);
			if (!PlayerPrefs.HasKey ("level2"))
				PlayerPrefs.SetInt ("level2", 0);
			if (!PlayerPrefs.HasKey ("level3"))
				PlayerPrefs.SetInt ("level3", 0);
			if (!PlayerPrefs.HasKey ("level4"))
				PlayerPrefs.SetInt ("level4", 0);
			if (!PlayerPrefs.HasKey ("unlock"))
				PlayerPrefs.SetInt ("unlock", 0);
			if (!PlayerPrefs.HasKey ("music"))
				PlayerPrefs.SetInt ("music", 1);
		} else if (type == ObjType.TEAPOT || type == ObjType.ELEPHANT || type == ObjType.FORTYTWO || type == ObjType.BONUSLVL){
			winGame = GameObject.FindGameObjectWithTag("winLevel") as GameObject;
			winGame.SetActive(false);
		}
	}
	
	void colorButton() {
		ColorBlock cb = new ColorBlock();
		GameObject[] tmp = GameObject.FindGameObjectsWithTag("buttonLevel");
		int i = 0;
		foreach (GameObject button in tmp) {
			levelList[i] = button.GetComponent<Button>();
			if (button.name == "ButtonLvl1" && PlayerPrefs.GetInt("level1") == 1) {
				cb = levelList[i].colors;
				cb.highlightedColor = Color.yellow;
				levelList[i].colors = cb;
			} else if (button.name == "ButtonLvl1" && PlayerPrefs.GetInt("level1") == 2) {
				cb = levelList[i].colors;
				cb.highlightedColor = Color.green;
				levelList[i].colors = cb;
			}
			if (button.name == "ButtonLvl2" && PlayerPrefs.GetInt("level2") == 0) {
				cb = levelList[i].colors;
				cb.highlightedColor = Color.red;
				levelList[i].colors = cb;
			} else if (button.name == "ButtonLvl2" && PlayerPrefs.GetInt("level2") == 1) {
				cb = levelList[i].colors;
				cb.highlightedColor = Color.yellow;
				levelList[i].colors = cb;
				cb = levelList[i].colors;
			} else if (button.name == "ButtonLvl2" && PlayerPrefs.GetInt("level2") == 2) {
				cb.highlightedColor = Color.green;
				levelList[i].colors = cb;
			}
			if (button.name == "ButtonLvl3" && PlayerPrefs.GetInt("level3") == 0) {
				cb = levelList[i].colors;
				cb.highlightedColor = Color.red;
				levelList[i].colors = cb;
			} else if (button.name == "ButtonLvl3" && PlayerPrefs.GetInt("level3") == 1) {
				cb = levelList[i].colors;
				cb.highlightedColor = Color.yellow;
				levelList[i].colors = cb;
			} else if (button.name == "ButtonLvl3" && PlayerPrefs.GetInt("level3") == 2) {
				cb = levelList[i].colors;
				cb.highlightedColor = Color.green;
				levelList[i].colors = cb;
			} else if (button.name == "ButtonLvl4" && PlayerPrefs.GetInt("level4") == 1) {
				cb = levelList[i].colors;
				cb.highlightedColor = Color.yellow;
				levelList[i].colors = cb;
			} else if (button.name == "ButtonLvl4" && PlayerPrefs.GetInt("level4") == 2) {
				cb = levelList[i].colors;
				cb.highlightedColor = Color.green;
				levelList[i].colors = cb;
			}
			
			if (PlayerPrefs.GetInt("test") == 1) {
				cb = levelList[i].colors;
				cb.highlightedColor = Color.yellow;
				levelList[i].colors = cb;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape))
			leaveLevel ();
		
		if (win == false) {
			if (rotationAllowed >= 3 && Input.GetMouseButton (0) && Input.GetKey (KeyCode.LeftShift))
				MoveSpace ();
			else if (rotationAllowed >= 2 && Input.GetMouseButton (0) && Input.GetKey (KeyCode.LeftControl))
				MoveVertical ();
			else if (rotationAllowed >= 1 && Input.GetMouseButton (0)) {
				MoveHorizontal ();
			} else
				current = 0;
			CheckWin ();
		}
	}
	
	public void leaveLevel() {
		if (type == ObjType.MENU)
			Application.LoadLevel ("TitleScreen");
		else if (type == ObjType.OPTION)
			Application.LoadLevel ("Option");
		else if (type == ObjType.MAINMENU)
			QuitGame ();
		else if (type == ObjType.TEAPOT || type == ObjType.ELEPHANT || type == ObjType.FORTYTWO)
			Application.LoadLevel("MainMenu");
	}
	
	void CheckWin() {
		if (type == ObjType.TEAPOT) {
			if ((obj.transform.eulerAngles.y >= 0 && obj.transform.eulerAngles.y < 3) || (obj.transform.eulerAngles.y >= 358 && obj.transform.eulerAngles.y <= 360))
				win = true;
		}

		if (win == true)
			DoWin ();
	}

	void DoWin() {
		win = true;
		winGame.SetActive(true);
		if (PlayerPrefs.GetInt ("test") == 0) {
			if (type == ObjType.TEAPOT) {
				PlayerPrefs.SetInt ("level1", 2);
				if (PlayerPrefs.GetInt ("level2") == 0) {
					PlayerPrefs.SetInt ("unlock", 2);
					PlayerPrefs.SetInt ("level2", 1);
				}
			} else if (type == ObjType.ELEPHANT) {
				PlayerPrefs.SetInt ("level2", 2);
				if (PlayerPrefs.GetInt ("level3") == 0) {
					PlayerPrefs.SetInt ("unlock", 3);
					PlayerPrefs.SetInt ("level3", 1);
				}
			} else if (type == ObjType.FORTYTWO) {
				PlayerPrefs.SetInt ("level3", 2);
				if (PlayerPrefs.GetInt ("level4") == 0) {
					PlayerPrefs.SetInt ("unlock", 4);
					PlayerPrefs.SetInt ("level4", 1);
				}
			} else if (type == ObjType.BONUSLVL) {
				PlayerPrefs.SetInt ("level4", 2);
			}
		}
	}

	int GetTarget() {
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		
		if (Physics.Raycast (ray, out hit)) {
			if (hit.collider.tag == "Player") {
				return (1);
			} else if (hit.collider.tag == "Player1") {
				return (2);
			}
		}
		return (0);
	}

	void MoveHorizontal() {
		float dir = Input.mousePosition.x - Screen.width / 2;
		if (type == ObjType.ELEPHANT) {
			obj.transform.Rotate (new Vector3 (0, 0, dir / 90), Space.World);
		} else if (type == ObjType.TEAPOT) {
			obj.transform.Rotate (new Vector3 (0, -dir / 90, 0), Space.World);
		} else if (type == ObjType.BONUSLVL) {
				obj.transform.Rotate(new Vector3 (0, -dir / 90, 0), Space.World);
		} else if (type == ObjType.FORTYTWO) {
			if (current == 0)
				current = GetTarget();
			if (current == 1)
				obj.transform.Rotate(new Vector3 (0, 0, dir / 90), Space.World);
			else if (current == 2)
				obj1.transform.Rotate(new Vector3 (0, 0, dir / 90), Space.World);
		}
	}
	
	void MoveVertical() {
		float dir = Input.mousePosition.y - Screen.height / 2;
		if (type == ObjType.ELEPHANT) {
			obj.transform.Rotate(new Vector3 (dir / 90, 0, 0), Space.World);
		} else if (type == ObjType.FORTYTWO) {
			if (current == 0)
				current = GetTarget();

			if (current == 1)
				obj.transform.Rotate(new Vector3 (-dir / 90, 0, 0), Space.World);
			else if (current == 2)
				obj1.transform.Rotate(new Vector3 (0, dir / 90, 0), Space.World);
		} else if (type == ObjType.BONUSLVL) {
			obj.transform.Rotate(new Vector3 (0, 0, -dir / 90), Space.World);
		}
	}
	
	void MoveSpace() {
		float dirx = Input.mousePosition.x - Screen.width / 2;
		obj.transform.position += new Vector3 (-dirx / 100, 0, 0) * Time.deltaTime;
	}

	public void ResetProgression() {
		PlayerPrefs.SetInt ("level1", 1);
		PlayerPrefs.SetInt ("level2", 0);
		PlayerPrefs.SetInt ("level3", 0);
		PlayerPrefs.SetInt ("level4", 0);
		PlayerPrefs.SetInt ("unlock", 0);
	}

	public void ReturnMenu() {
		Application.LoadLevel ("MainMenu");
	}
	
	public void QuitGame() {
		Application.Quit ();
	}
	
	public void LaunchTest() {
		PlayerPrefs.SetInt("test", 1);
		Application.LoadLevel ("MainMenu");
	}
	
	public void LaunchClassic() {
		PlayerPrefs.SetInt("test", 0);
		Application.LoadLevel ("MainMenu");
	}
	
	public void LaunchLevel(int level) {
		if (level == 1 && (PlayerPrefs.GetInt("test") == 1 || PlayerPrefs.GetInt("level1") >= 1))
			Application.LoadLevel("teapot");
		else if (level == 2 && (PlayerPrefs.GetInt("test") == 1 || PlayerPrefs.GetInt("level2") >= 1))
			Application.LoadLevel("elephant");
		else if (level == 3 && (PlayerPrefs.GetInt("test") == 1 || PlayerPrefs.GetInt("level3") >= 1))
			Application.LoadLevel("globe");
		else if (level == 4 && (PlayerPrefs.GetInt("test") == 1 || PlayerPrefs.GetInt("level4") >= 1))
			Application.LoadLevel("42");
		
	}

	public void ChangeSounds(int state) {
		PlayerPrefs.SetInt ("music", state);
	}

	public void DetectionChange(int change) {
		detection += change;
		if (detection == maxDetection) {
			DoWin();
		}

	}
}
