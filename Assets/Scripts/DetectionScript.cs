﻿using UnityEngine;
using System.Collections;

public class DetectionScript : MonoBehaviour {

	private GameManager manager;

	// Use this for initialization
	void Start () {
		manager = GameObject.Find ("Manager").GetComponent<GameManager> ();
	}
	

	void OnTriggerEnter(Collider collide) {
		manager.DetectionChange (1);
	}

	void OnTriggerExit(Collider collide) {
		manager.DetectionChange (-1);
	}
}
